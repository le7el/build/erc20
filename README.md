# LE7EL Tokens

Frontend API to interact with Token contracts on any EVM chain.

## Installation
```sh
npm install @le7el/tokens
```

Keep in mind that `ethers` package should be available within your build, which uses this library.

## Getting started
```js
import { erc20, erc1155 } from '@le7el/tokens'
```

## ERC20 Utils
This token stadard is most commonly used
[Read more](https://ethereum.org/ru/developers/docs/standards/tokens/erc-20/)

#### erc20.humanReadable(token: string, amount: BigNumber, decimalPrecision: number, web3Provider)
Rounds given token amount to desired precision and returns utility properties of token to be used for further formating.

```js
const token = '0x8F2Ac3fD1a9Ce7208eFff7C31aC0e2A98b0778f3';   // Token address.
const amount = BigNumber.from('198069699999788797999');       // BigNumber without decimals.
const decimalPrecision = null;                                // Count of significant decimals. Default: 2
const provider = null;                                        // Web3 provider. Default: window.ethereum

erc20.humanReadable(token, amount, decimalPrecision, provider).then(console.log)
// {symbol: 'DAI', amount: 198.06, image: 'https://assets.coingecko.com/coins/images/9956/thumb/4943.png'}
```

#### erc20.getBalance(token: string, holderAddress: string, web3Provider): Promise< BigNumber >
```js
const token = '0x8F2Ac3fD1a9Ce7208eFff7C31aC0e2A98b0778f3';   // Token address.
const provider = null;                                        // Web3 provider. Default: `window.ethereum`
const holder = "0x5853ed4f26a3fcea565b3fbc698bb19cdf6deb85"   // Holder address

erc20.getBalance(token, holder, provider).then(console.log)
// K {_hex: '0x00', _isBigNumber: true}
```

#### erc20.tokenImage(symbol: string, size: string)
```js
const symbol = 'DAI';   // Token symbol
const size = null;      // Image size from Coingecko API ('thumb' | 'large'). Default: 'thumb'

erc20.tokenImage(symbol, size).then(console.log)
// 'https://assets.coingecko.com/coins/images/9956/thumb/4943.png'
```

#### erc20.symbol(token: string, web3Provider): Promise< string >
```js
const token = '0x8F2Ac3fD1a9Ce7208eFff7C31aC0e2A98b0778f3';   // Token address.
const provider = null;                                        // Web3 provider. Default: `window.ethereum`
erc20.symbol(token, provider).then(console.log)
// "DAI"
```

#### erc20.decimals(token: string, web3Provider): Promise< BigNumber >
```js
const token = '0x8F2Ac3fD1a9Ce7208eFff7C31aC0e2A98b0778f3';   // Token address.
const provider = null;                                        // Web3 provider. Default: `window.ethereum`
erc20.symbol(token, provider).then(console.log)
// K {_hex: '0x12', _isBigNumber: true}
```

#### erc20.applyPrecision(amount: number, decimalPrecision: number);
Function that formats decimals inside humanReadable function 
```js
erc20.applyPrecision(0.1009, 3);                // 0.1
erc20.applyPrecision(0.009999, 3);              // 0.00999
erc20.applyPrecision(1.009999, 2);              // 1
erc20.applyPrecision(1.009999, 3);              // 1.009
```

## ERC1155 Utils
This token standard stores multiple tokens within same contract, so tokenIndex should be specified
[Read more](https://ethereum.org/ru/developers/docs/standards/tokens/erc-1155/)
#### erc1155.getBalance(token: string, holderAddress: string, tokenIndex: number, web3Provider): Promise< BigNumber >
```js
const token = '0xE4d52aa9d5b1dCBa0Ec24CA6B7305a609167Bb3e';   // Token address.
const provider = null;                                        // Web3 provider. Default: `window.ethereum`
const holder = "0x5853ed4f26a3fcea565b3fbc698bb19cdf6deb85"   // Holder address
const tokenIndex = 0                                          // Specific token index

erc1155.getBalance(token, holder, tokenIndex, provider).then(console.log)
// K {_hex: '0x00', _isBigNumber: true}
```