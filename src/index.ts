
import { BigNumber } from 'ethers'
import { humanReadable } from './erc20'

const displayResult = function (data: any) {
  const amount = document.getElementById('js-amount') as HTMLElement | null
  if (amount)
    amount.innerText = data.amount

  const symbol = document.getElementById('js-token-symbol') as HTMLElement | null
  if (symbol)
    symbol.innerText = data.symbol

  const image = document.getElementById('js-token-image') as HTMLElement | null
  image?.setAttribute('src', data.image)
}

document.getElementById('js-human-readable')?.addEventListener('submit', (event) => {
  event.preventDefault()
  if (!event.target) return

  const formData = new FormData(event.target as HTMLFormElement)
  const amount = BigNumber.from(formData.get("amount"))
  const token = formData.get("token")?.toString()
  const precision = formData.get("precision")?.toString()
  if (!token || !amount || !precision) return

  humanReadable(token, amount, parseInt(precision)).then((res: any) => {
    console.log('humanReadable', res)
    displayResult(res)
  })

  return false
})