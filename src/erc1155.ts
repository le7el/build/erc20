import { BigNumber } from "ethers"
import { initContract } from "./common"

const abi = [
  "function balanceOf(address, uint256) view returns (uint256)",
]

const getBalance = async function (tokenAddress: string, holderAddress: string, tokenIndex: number, web3Provider = null): Promise<BigNumber> {
  const contract = await initContract(tokenAddress, abi, web3Provider)

  return contract.balanceOf(holderAddress, tokenIndex)
}

export {
  getBalance,
}
