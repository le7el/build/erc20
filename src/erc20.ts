import { ethers, BigNumber } from "ethers"
import { initContract } from "./common"

const abi = [
  "function name() view returns (string)",
  "function symbol() view returns (string)",
  "function decimals() view returns (uint)",
  "function balanceOf(address) view returns (uint256)",
]

enum tokenImageSize {
  THUMB = 'thumb',
  LARGE = 'large'
}

const symbol = (tokenAddress: string, web3Provider = null): Promise<string> => {
  return initContract(tokenAddress, abi, web3Provider)
    .then(contract => contract.symbol())
}

const decimals = (tokenAddress: string, web3Provider = null): Promise<BigNumber> => {
  return initContract(tokenAddress, abi, web3Provider)
    .then(contract => contract.decimals())
}

const tokenImage = function (symbol: string, size: tokenImageSize = tokenImageSize.THUMB): Promise<string | false> {
  return fetch(`https://api.coingecko.com/api/v3/search?query=${symbol}`)
    .then(response => response.json())
    .then(data => {
      return data?.coins?.find(function (coin: any, idx: number, array: []) {
        return coin.symbol === symbol;
      })[size] || false
    })
}

const applyPrecision = function (amount: number, decimalPrecision: number = 0): number {
  let matchPrecision, decimalZero;
  if (!amount) return 0
  if (amount < 1) {
    matchPrecision = amount.toString().match(/\.(0+)/)
    decimalZero = matchPrecision ? matchPrecision[1].length : 0
    return Math.trunc(amount * Math.pow(10, decimalPrecision + decimalZero)) / Math.pow(10, decimalPrecision + decimalZero)
  }
  return Math.floor(amount * Math.pow(10, decimalPrecision)) / Math.pow(10, decimalPrecision)
}

type HumanReadable = {
  symbol: string,
  amount: number,
  image: string | false,
}

const humanReadable = function (tokenAddress: string, fullAmount: BigNumber, decimalPrecision: number = 2, web3Provider = null)
  : Promise<HumanReadable> {
  return Promise.all([
    symbol(tokenAddress, web3Provider),
    decimals(tokenAddress, web3Provider)
  ])
    .then(([symbol, decimals]) => {
      const amount = applyPrecision(parseFloat(ethers.utils.formatUnits(fullAmount, decimals.toNumber())), decimalPrecision)
      return tokenImage(symbol)
        .then((image) => { return { symbol, amount, image } })
    })
}

const getBalance = async function (tokenAddress: string, holderAddress: string, web3Provider = null): Promise<BigNumber> {
  const contract = await initContract(tokenAddress, abi, web3Provider)
  return contract.balanceOf(holderAddress)
}

export {
  humanReadable,
  applyPrecision,
  tokenImage,
  symbol,
  decimals,
  getBalance,
}