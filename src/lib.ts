import {
  humanReadable,
  tokenImage,
  applyPrecision,
  symbol,
  decimals,
  getBalance as getErc20Balance,
} from './erc20'
import { getBalance as getErc1155Balance } from './erc1155'

const erc20 = {
  humanReadable,
  tokenImage,
  applyPrecision,
  symbol,
  decimals,
  getBalance: getErc20Balance,
}

const erc1155 = {
  getBalance: getErc1155Balance,
}

export {
  erc20,
  erc1155,
}