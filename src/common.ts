import { ethers, type ContractInterface } from "ethers";

declare global {
  interface Window {
    ethereum: ethers.providers.ExternalProvider | ethers.providers.JsonRpcFetchFunc;
  }
}

const getWeb3Provider = (provider: ethers.providers.Web3Provider | null = null): Promise<ethers.providers.Web3Provider> => {
  if (provider) return Promise.resolve(provider)
  if (window.ethereum) return Promise.resolve(new ethers.providers.Web3Provider(window.ethereum, "any"))
  return Promise.reject('no web3 provider defined')
}

const initContract = async (
  tokenAddress: string,
  abi: ContractInterface,
  web3Provider: ethers.providers.Web3Provider | null = null
): Promise<ethers.Contract> => {
  const provider = await getWeb3Provider(web3Provider);
  return new ethers.Contract(tokenAddress, abi, provider);
}

export {
  initContract,
}
